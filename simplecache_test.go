package simplecache

import (
	"sync"
	"sync/atomic"
	"testing"
	"time"
)

func TestCache(t *testing.T) {
	t.Logf("Initial fill")
	c := New[int64, int64](16, nil)
	for i := int64(0); i < 100; i++ {
		c.Acquire(i, func(k int64) (int64, error) { return k, nil })
	}
	t.Logf("Filled")
	for i := int64(0); i < 100; i++ {
		c.Release(i)
	}
}

func TestCacheParallel(t *testing.T) {
	c := New[int, int](16, nil)
	times := 256
	par := 64
	calls := times * par
	called := int64(0)
	t.Logf("Adding %d items %d times in parallel (%d goroutines)", par, times, calls)
	wg := &sync.WaitGroup{}
	wg.Add(calls)
	add := func(k int) (int, error) {
		time.Sleep(time.Second)
		atomic.AddInt64(&called, 1)
		return k, nil
	}
	for i := 0; i < calls; i++ {
		go func(v int) {
			c.Acquire(v, add)
			wg.Done()
		}(i % par)
	}
	wg.Wait()
	if int(called) != par {
		t.Errorf("add called %d times instead of %d", called, par)
	}
	for i := calls - 1; i >= 0; i-- {
		c.Release(i % par)
	}
	c.Clear()
}
