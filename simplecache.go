// Package simplecache implements a simple map-like cache for
// static values that are generally expensive to calculate on demand.
package simplecache

import (
	"sync"
	"time"
)

// Fill is called if there is no value for K already present in
// the Cache. It should return a value and an error if there was a problem
// generating the value. Get requests for K will block until Fill has returned
// either a value or an error.
type Fill[K comparable, T any] func(K) (T, error)

// Cleanup is called when a value is evicted from the cache.  It should
// perform any extra actions needed to clean up the value that is being released.
type Cleanup[T any] func(T)

// cacheLine contains the cached value and the number of times it
// is being concurrently actively used.
type cacheLine[T any] struct {
	useCount int64
	val      T
}

// waitLine is used to guarantee that the cache only calculates
// a new value for a given key once no matter how many parallel calls
// are made to fetch the value from different goroutines. It also
// allows for returning any errors returned by Fill
type waitLine[T any] struct {
	sync.WaitGroup
	err error
	*cacheLine[T]
}

// Cache is a concurrency safe cache with a limited number slots for stale data.
// When in use, Cache will hold a reference to every item calculated item that
// has not been evicted.
type Cache[K comparable, T any] struct {
	mu              *sync.Mutex
	pending         map[K]*waitLine[T]
	calculated      map[K]*cacheLine[T]
	evictCandidates []K
	cleaner         Cleanup[T]
	maxStaleEntries int
	lastRelease     time.Time
}

// NewCache returns a Cache with a specified number of slots for data that is no longer
// in use. A general guideline is that maxStale should be ln(expected data).
func New[K comparable, T any](maxStale int, cleaner Cleanup[T]) *Cache[K, T] {
	return &Cache[K, T]{
		mu:              &sync.Mutex{},
		maxStaleEntries: maxStale,
		pending:         map[K]*waitLine[T]{},
		calculated:      map[K]*cacheLine[T]{},
		cleaner:         cleaner,
		evictCandidates: make([]K, 0, maxStale),
	}
}

func (bc *Cache[K, T]) sweeper() {
	for {
		time.Sleep(time.Second)
		bc.mu.Lock()
		if len(bc.evictCandidates) == 0 {
			bc.lastRelease = time.Time{}
			bc.mu.Unlock()
			return
		}
		if bc.lastRelease.After(time.Now()) {
			bc.mu.Unlock()
			continue
		}
		toRemove := bc.calculated[bc.evictCandidates[0]].val
		delete(bc.calculated, bc.evictCandidates[0])
		bc.evictCandidates = append(bc.evictCandidates[:0], bc.evictCandidates[1:]...)
		bc.mu.Unlock()
		if bc.cleaner != nil {
			bc.cleaner(toRemove)
		}
	}
}

// rehabilitate marks key as not being a candidate for eviction.
func (bc *Cache[K, T]) rehabilitate(key K) {
	for pos := range bc.evictCandidates {
		if bc.evictCandidates[pos] != key {
			continue
		}
		if pos+1 < len(bc.evictCandidates) {
			copy(bc.evictCandidates[pos:], bc.evictCandidates[pos+1:])
		}
		bc.evictCandidates = bc.evictCandidates[:len(bc.evictCandidates)-1]
		break
	}
}

func (bc *Cache[K, T]) get(key K) (ent *cacheLine[T], err error) {
	if wait := bc.pending[key]; wait != nil {
		wait.cacheLine.useCount++
		bc.mu.Unlock()
		wait.Wait()
		ent, err = wait.cacheLine, wait.err
	} else if ent = bc.calculated[key]; ent != nil {
		if ent.useCount == 0 {
			bc.rehabilitate(key)
		}
		ent.useCount++
		bc.mu.Unlock()
	}
	return
}

func (bc *Cache[K, T]) acquire(key K, fill Fill[K, T]) (T, error) {
	bc.mu.Lock()
	if ent, err := bc.get(key); ent != nil {
		// get will have already unlocked the mux and waited, so return.
		return ent.val, err
	}
	wait := &waitLine[T]{cacheLine: &cacheLine[T]{useCount: 1}}
	wait.Add(1)
	bc.pending[key] = wait
	bc.mu.Unlock()
	wait.val, wait.err = fill(key)
	bc.mu.Lock()
	delete(bc.pending, key)
	if wait.err == nil {
		bc.calculated[key] = wait.cacheLine
	}
	bc.mu.Unlock()
	wait.Done()
	return wait.val, wait.err
}

// Acquire fetches the cached value for key.  If no such value is present
// in the Cache, fill will be called exactly once even if Acquire(key,fill) is called
// from multiple goroutines at the same time. fill is called with all locks
// dropped, so it is safe for fill to take a long time or block on other operations.
//
// Each call to Acquire(key) that does not return an error must be matched by an equivalent
// call to Release(key), or the cache will grow without bounds. You must not call Release(key)
// if Acquire(key) returns an error.
func (bc *Cache[K, T]) Acquire(key K, fill Fill[K, T]) (T, error) {
	return bc.acquire(key, fill)
}

// Release is called to indicate that the caller is no longer
// interested in the cached value for key. When there is no interest
// left in the value, it will become eligible for eviction from the cache.
// When there are no free slots for stale data, Release will release the least
// recently used half of the stale slots to free up space.
func (bc *Cache[K, T]) Release(keys ...K) {
	var toRelease []T
	if bc.cleaner != nil {
		defer func() {
			for i := range toRelease {
				bc.cleaner(toRelease[i])
			}
		}()
	}
	bc.mu.Lock()
	defer bc.mu.Unlock()
	for _, key := range keys {
		ent := bc.calculated[key]
		if ent == nil || ent.useCount == 0 {
			panic("Bad Release")
		}
		ent.useCount--
		if ent.useCount > 0 {
			continue
		}
		if bc.maxStaleEntries == 0 {
			continue
		}
		if toEvict := len(bc.evictCandidates) - bc.maxStaleEntries; toEvict > 0 {
			for i := range bc.evictCandidates[:toEvict] {
				discard := bc.calculated[bc.evictCandidates[i]]
				toRelease = append(toRelease, discard.val)
				delete(bc.calculated, bc.evictCandidates[i])
			}
			bc.evictCandidates = append(bc.evictCandidates[:0], bc.evictCandidates[toEvict:]...)
		}
		if len(bc.evictCandidates) == 0 && bc.lastRelease.Equal(time.Time{}) {
			go bc.sweeper()
		}
		bc.evictCandidates = append(bc.evictCandidates, key)
		bc.lastRelease = time.Now().Add(time.Second)
	}
}

// Clear unilaterally releases all stale data in the cache.
// Live data is unaffected.
func (bc *Cache[K, T]) Clear() {
	bc.mu.Lock()
	if len(bc.evictCandidates) == 0 {
		bc.mu.Unlock()
		return
	}
	toRelease := make([]T, len(bc.evictCandidates))
	for i := range bc.evictCandidates {
		toRelease[i] = bc.calculated[bc.evictCandidates[i]].val
		delete(bc.calculated, bc.evictCandidates[i])
	}
	bc.evictCandidates = bc.evictCandidates[:0]
	if bc.cleaner != nil {
		defer func() {
			for i := range toRelease {
				bc.cleaner(toRelease[i])
			}
		}()
	}
	bc.mu.Unlock()
}
